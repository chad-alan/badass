(function($) {
	
		
	$(document).ready(function(){
		
		$('label.placeholder').on('click', function()
		{
			$(this).parents('.input').find('input').focus();
		});
		
		$('.input input').on('keyup', function()
		{
			var val = $(this).val();
			if(val == '' || val == undefined) {
				$(this).parents('.input').find('label.placeholder').show();
			}else{
				$(this).parents('.input').find('label.placeholder').hide();
			}
		});
		
	});
	
})(jQuery);